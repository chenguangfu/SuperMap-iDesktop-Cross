package com.supermap.desktop.CtrlAction.property;

import javax.swing.JFrame;

import com.supermap.desktop.Application;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.dataview.propertycontrols.JDialogDataPropertyContainer;
import com.supermap.desktop.implement.CtrlAction;

public class CtrlActionDataProperty extends CtrlAction {

	private JDialogDataPropertyContainer dataPropertyContainer = null;

	public CtrlActionDataProperty(IBaseItem caller, IForm formClass) {
		super(caller, formClass);

	}

	@Override
	public void run() {
		if (dataPropertyContainer == null) {
			dataPropertyContainer = new JDialogDataPropertyContainer((JFrame) Application.getActiveApplication().getMainFrame());
		}

		dataPropertyContainer.setVisible(true);
	}
}
