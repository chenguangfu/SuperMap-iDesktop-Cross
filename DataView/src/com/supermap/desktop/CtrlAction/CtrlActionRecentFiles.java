package com.supermap.desktop.CtrlAction;

import java.awt.Component;
import java.io.File;

import javax.swing.JOptionPane;

import com.supermap.data.Datasource;
import com.supermap.data.WorkspaceConnectionInfo;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.enums.OpenWorkspaceResult;
import com.supermap.desktop.implement.CtrlAction;
import com.supermap.desktop.implement.SmMenuItem;
import com.supermap.desktop.properties.CoreProperties;
import com.supermap.desktop.ui.UICommonToolkit;

public class CtrlActionRecentFiles extends CtrlAction {

	public CtrlActionRecentFiles(IBaseItem caller, IForm formClass) {
		super(caller, formClass);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		try {
			if (this.getCaller() instanceof SmMenuItem) {
				String filePath = ((SmMenuItem) this.getCaller()).getToolTipText();
				File file = new File(filePath);
				if (file.exists()) {
					if (isWorkSpaceFile(file)) {
						WorkspaceConnectionInfo connectionInfo = new WorkspaceConnectionInfo(file.getAbsolutePath());

						OpenWorkspaceResult result = CommonToolkit.WorkspaceWrap.openWorkspace(connectionInfo, true);
						if (result == OpenWorkspaceResult.SUCCESSED) {
							if (Application.getActiveApplication().getMainFrame().getFormManager().getCount() > 0) {
								Application.getActiveApplication().getMainFrame().getFormManager().closeAll();
							}
						} else {
							while (result == OpenWorkspaceResult.FAILED_PASSWORD_WRONG) {
								Component parent = (Component) Application.getActiveApplication().getMainFrame();
								String inputPassword = (String) JOptionPane.showInputDialog(parent, CoreProperties.getString("String_WorkspacePasswordPrompt"),
										CoreProperties.getString("String_Prompt"), JOptionPane.QUESTION_MESSAGE);
								if (inputPassword != null)// 按取消按钮不执行打开工作空间的操作。
								{
									connectionInfo.setPassword(inputPassword);
									result = CommonToolkit.WorkspaceWrap.openWorkspace(connectionInfo, false);
								} else {
									result = OpenWorkspaceResult.FAILED_CANCEL;
								}
							}
						}

						if (result != OpenWorkspaceResult.SUCCESSED) {
							String stMsg = String.format(CoreProperties.getString("String_OpenWorkspaceFailed"), filePath);
							if (result != OpenWorkspaceResult.FAILED_CANCEL) {
								Application.getActiveApplication().getWorkspace().close();
							} else if (result == OpenWorkspaceResult.FAILED_PASSWORD_WRONG) {
								stMsg = String.format(CoreProperties.getString("String_OpenWorkspaceFailed_WrongPassword"), filePath);
							}
							Application.getActiveApplication().getOutput().output(stMsg);
						}
					} else {
						Datasource datasource = CommonToolkit.DatasourceWrap.openFileDatasource(file.getAbsolutePath(), null, false);
						if (null != datasource) {
							UICommonToolkit.refreshSelectedDatasourceNode(datasource.getAlias());
						}
					}
				} else {
					// 提示文件不存在，删除最近文件项
					UICommonToolkit.showMessageDialog("文件不存在");

					if (isWorkSpaceFile(file)) {
						CommonToolkit.WorkspaceWrap.removeRecentFile(filePath);
					} else {
						CommonToolkit.DatasourceWrap.removeRecentFile(filePath);
					}
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private boolean isWorkSpaceFile(File file) {
		boolean flag = false;
		String fileName = file.getName();
		String fileType = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
		if ("smwu".equalsIgnoreCase(fileType) || "sxwu".equalsIgnoreCase(fileType)) {
			flag = true;
		}
		return flag;
	}

}
