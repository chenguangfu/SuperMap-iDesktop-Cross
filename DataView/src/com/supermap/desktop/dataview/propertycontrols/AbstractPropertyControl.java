package com.supermap.desktop.dataview.propertycontrols;

import javax.swing.JDialog;
import javax.swing.JPanel;

public abstract class AbstractPropertyControl extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String propertyName = "";
	private JDialog container = null;

	/**
	 * Create the panel.
	 */
	protected AbstractPropertyControl(String propertyName, JDialog container) {
		this.propertyName = propertyName;
		this.container = container;
	}

	public final String getPropertyName() {
		return this.propertyName;
	}

	public final JDialog getContainer() {
		return this.container;
	}
}
