package com.supermap.desktop.dataview.propertycontrols;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.text.NumberFormatter;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.supermap.data.Charset;
import com.supermap.data.DatasetVector;
import com.supermap.data.EngineType;
import com.supermap.data.SpatialIndexType;
import com.supermap.data.Tolerance;
import com.supermap.desktop.Application;
import com.supermap.desktop.controls.ControlDefaultValues;
import com.supermap.desktop.dataview.DataViewProperties;
import com.supermap.desktop.properties.CommonProperties;
import com.supermap.desktop.properties.CoreProperties;
import com.supermap.desktop.ui.controls.comboBox.ComboBoxCharset;
import com.supermap.desktop.utilties.SpatialIndexTypeUtilties;

public class VectorPropertyControl extends AbstractPropertyControl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int DEFAULT_LABEL_WIDTH = 100;
	private static final int DEFAULT_COMPONENT_WIDTH = 150;

	private JLabel labelRecordCount;
	private JTextField textFieldRecordCount;
	private JLabel labelSpatialIndexType;
	private JTextField textFieldSpatialIndexType;
	private JLabel labelCharset;
	private ComboBoxCharset comboBoxCharset;
	private JCheckBox checkBoxIsReadOnly;
	private JCheckBox checkBoxIsFileCache;
	private JButton buttonClearCache;

	private JLabel labelNodeSnap;
	private JFormattedTextField textFieldNodeSnap;
	private JLabel labelDangle;
	private JFormattedTextField textFieldDangle;
	private JLabel labelGrain;
	private JFormattedTextField textFieldGrain;
	private JLabel labelExtend;
	private JFormattedTextField textFieldExtend;
	private JLabel labelSmallPolygon;
	private JFormattedTextField textFieldSmallPolygon;
	private JButton buttonDefaultTolerance;
	private JButton buttonClearTolerance;

	private JButton buttonReset;
	private JButton buttonApply;

	private transient DatasetVector datasetVector;

	private transient Charset charset = Charset.DEFAULT; // 字符集
	private boolean isReadOnly = false; // 只读
	private boolean isFileCache = false; // 用户缓存
	private double nodeSnap = 0.0; // 节点容限
	private double grain = 0.0; // 颗粒容限
	private double smallPolygon = 0.0; // 多边形容限
	private double dangle = 0.0; // 短悬线容限
	private double extend = 0.0; // 长悬线容限

	private transient ItemListener itemListener = new ItemListener() {

		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getSource() == comboBoxCharset) {
				comboBoxCharsetSelectedChange(e);
			} else if (e.getSource() == checkBoxIsReadOnly) {
				checkBoxIsReadOnlyCheckedChange();
			} else if (e.getSource() == checkBoxIsFileCache) {
				checkBoxIsFileCacheCheckedChange();
			}
		}
	};
	private transient PropertyChangeListener propertyChangeListener = new PropertyChangeListener() {

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if (evt.getSource() == textFieldNodeSnap) {
				textFieldNodeSnapValueChange();
			} else if (evt.getSource() == textFieldGrain) {
				textFieldGrainValueChange();
			} else if (evt.getSource() == textFieldSmallPolygon) {
				textFieldSmallPolygonValueChange();
			} else if (evt.getSource() == textFieldDangle) {
				textFieldDangleValueChange();
			} else if (evt.getSource() == textFieldExtend) {
				textFieldExtendValueChange();
			}
		}
	};
	private transient ActionListener actionListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == buttonClearCache) {
				buttonClearCacheClicked();
			} else if (e.getSource() == buttonDefaultTolerance) {
				buttonDefaultToleranceClicked();
			} else if (e.getSource() == buttonClearTolerance) {
				buttonClearToleranceClicked();
			} else if (e.getSource() == buttonReset) {
				buttonResetClicked();
			} else if (e.getSource() == buttonApply) {
				buttonApplyClicked();
			}
		}
	};

	public VectorPropertyControl(DatasetVector datasetVector, JDialog container) {
		super(DataViewProperties.getString("String_DatasetVector"), container);
		initializeComponents();
		initializeResources();
		setDatasetVector(datasetVector);
	}

	public DatasetVector getDatasetVector() {
		return datasetVector;
	}

	public void setDatasetVector(DatasetVector datasetVector) {
		this.datasetVector = datasetVector;
		unregisterEvents();
		reset();
		fillComponents();
		setComponentsEnabled();
		registerEvents();
	}

	private void initializeComponents() {
		this.labelRecordCount = new JLabel("RecordCount:");
		this.textFieldRecordCount = new JTextField();
		this.textFieldRecordCount.setEditable(false);
		this.labelSpatialIndexType = new JLabel("SpatialIndexType:");
		this.textFieldSpatialIndexType = new JTextField();
		this.textFieldSpatialIndexType.setEditable(false);
		this.labelCharset = new JLabel("Charset:");
		this.comboBoxCharset = new ComboBoxCharset();
		this.checkBoxIsReadOnly = new JCheckBox("IsReadOnly");
		this.checkBoxIsFileCache = new JCheckBox("IsFileCache");
		this.buttonClearCache = new JButton("ClearCache");

		JPanel panelVectorParam = new JPanel();
		panelVectorParam.setBorder(BorderFactory.createTitledBorder(DataViewProperties.getString("String_DatasetVector")));
		GroupLayout gl_panelVectorParam = new GroupLayout(panelVectorParam);
		gl_panelVectorParam.setAutoCreateContainerGaps(true);
		gl_panelVectorParam.setAutoCreateGaps(true);
		panelVectorParam.setLayout(gl_panelVectorParam);
		// @formatter:off
		gl_panelVectorParam.setHorizontalGroup(gl_panelVectorParam.createSequentialGroup()
				.addGroup(gl_panelVectorParam.createParallelGroup(Alignment.LEADING)
						.addComponent(this.labelRecordCount, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH)
						.addComponent(this.labelCharset, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH))
				.addGroup(gl_panelVectorParam.createParallelGroup(Alignment.LEADING)
						.addComponent(this.textFieldRecordCount, DEFAULT_COMPONENT_WIDTH, DEFAULT_COMPONENT_WIDTH, DEFAULT_COMPONENT_WIDTH)
						.addComponent(this.comboBoxCharset, DEFAULT_COMPONENT_WIDTH, DEFAULT_COMPONENT_WIDTH, DEFAULT_COMPONENT_WIDTH))
				.addGroup(gl_panelVectorParam.createParallelGroup(Alignment.LEADING)
						.addComponent(this.labelSpatialIndexType, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH)
						.addComponent(this.checkBoxIsReadOnly, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH))
				.addGroup(gl_panelVectorParam.createParallelGroup(Alignment.LEADING)
						.addComponent(this.textFieldSpatialIndexType, GroupLayout.PREFERRED_SIZE, DEFAULT_COMPONENT_WIDTH, Short.MAX_VALUE)
						.addGroup(gl_panelVectorParam.createSequentialGroup()
								.addComponent(this.checkBoxIsFileCache)
								.addGap(10, 20, Short.MAX_VALUE)
								.addComponent(this.buttonClearCache))));
		
		gl_panelVectorParam.setVerticalGroup(gl_panelVectorParam.createSequentialGroup()
				.addGroup(gl_panelVectorParam.createParallelGroup(Alignment.CENTER)
						.addComponent(this.labelRecordCount)
						.addComponent(this.textFieldRecordCount, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(this.labelSpatialIndexType)
						.addComponent(this.textFieldSpatialIndexType, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelVectorParam.createParallelGroup(Alignment.CENTER)
						.addComponent(this.labelCharset)
						.addComponent(this.comboBoxCharset, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(this.checkBoxIsReadOnly)
						.addComponent(this.checkBoxIsFileCache)
						.addComponent(this.buttonClearCache)));
		// @formatter:on

		NumberFormatter numberFormatter = new NumberFormatter(NumberFormat.getNumberInstance());
		numberFormatter.setValueClass(Double.class);
		numberFormatter.setMinimum(0.0);
		this.labelNodeSnap = new JLabel("NodeSnap:");
		this.textFieldNodeSnap = new JFormattedTextField(numberFormatter);
		this.labelDangle = new JLabel("Dangle");
		this.textFieldDangle = new JFormattedTextField(numberFormatter);
		this.labelExtend = new JLabel("Extend");
		this.textFieldExtend = new JFormattedTextField(numberFormatter);
		this.labelGrain = new JLabel("Grain:");
		this.textFieldGrain = new JFormattedTextField(numberFormatter);
		this.labelSmallPolygon = new JLabel("SmallPolygon:");
		this.textFieldSmallPolygon = new JFormattedTextField(numberFormatter);
		this.buttonDefaultTolerance = new JButton("Default");
		this.buttonClearTolerance = new JButton("Clear");

		JPanel panelTolerance = new JPanel();
		panelTolerance.setBorder(BorderFactory.createTitledBorder(DataViewProperties.getString("String_DatasetTolerance")));
		GroupLayout gl_panelTolerance = new GroupLayout(panelTolerance);
		gl_panelTolerance.setAutoCreateContainerGaps(true);
		gl_panelTolerance.setAutoCreateGaps(true);
		panelTolerance.setLayout(gl_panelTolerance);
		// @formatter:off
		gl_panelTolerance.setHorizontalGroup(gl_panelTolerance.createSequentialGroup()
				.addGroup(gl_panelTolerance.createParallelGroup(Alignment.LEADING)
						.addComponent(this.labelNodeSnap, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH)
						.addComponent(this.labelGrain, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH)
						.addComponent(this.labelSmallPolygon, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH))
				.addGroup(gl_panelTolerance.createParallelGroup(Alignment.LEADING)
						.addComponent(this.textFieldNodeSnap, DEFAULT_COMPONENT_WIDTH, DEFAULT_COMPONENT_WIDTH, DEFAULT_COMPONENT_WIDTH)
						.addComponent(this.textFieldGrain, DEFAULT_COMPONENT_WIDTH, DEFAULT_COMPONENT_WIDTH, DEFAULT_COMPONENT_WIDTH)
						.addComponent(this.textFieldSmallPolygon, DEFAULT_COMPONENT_WIDTH, DEFAULT_COMPONENT_WIDTH, DEFAULT_COMPONENT_WIDTH))
				.addGroup(gl_panelTolerance.createParallelGroup(Alignment.LEADING)
						.addComponent(this.labelDangle, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH)
						.addComponent(this.labelExtend, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_WIDTH))
				.addGroup(gl_panelTolerance.createParallelGroup(Alignment.LEADING)
						.addComponent(this.textFieldDangle, GroupLayout.PREFERRED_SIZE, DEFAULT_COMPONENT_WIDTH, Short.MAX_VALUE)
						.addComponent(this.textFieldExtend, GroupLayout.PREFERRED_SIZE, DEFAULT_COMPONENT_WIDTH, Short.MAX_VALUE)
						.addGroup(gl_panelTolerance.createSequentialGroup()
								.addGap(10, 10, Short.MAX_VALUE)
								.addComponent(this.buttonDefaultTolerance, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(this.buttonClearTolerance, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))));
		
		gl_panelTolerance.setVerticalGroup(gl_panelTolerance.createSequentialGroup()
				.addGroup(gl_panelTolerance.createParallelGroup(Alignment.CENTER)
						.addComponent(this.labelNodeSnap)
						.addComponent(this.textFieldNodeSnap, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(this.labelDangle)
						.addComponent(this.textFieldDangle, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelTolerance.createParallelGroup(Alignment.CENTER)
						.addComponent(this.labelGrain)
						.addComponent(this.textFieldGrain, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(this.labelExtend)
						.addComponent(this.textFieldExtend, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelTolerance.createParallelGroup(Alignment.CENTER)
						.addComponent(this.labelSmallPolygon)
						.addComponent(this.textFieldSmallPolygon, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(this.buttonDefaultTolerance)
						.addComponent(this.buttonClearTolerance)));
		// |@formatter:on

		this.buttonReset = new JButton("Reset");
		this.buttonApply = new JButton("Apply");
		GroupLayout gl_mainContent = new GroupLayout(this);
		gl_mainContent.setAutoCreateContainerGaps(true);
		gl_mainContent.setAutoCreateGaps(true);
		this.setLayout(gl_mainContent);
		// @formatter:off
		gl_mainContent.setHorizontalGroup(gl_mainContent.createParallelGroup(Alignment.LEADING)
				.addComponent(panelVectorParam)
				.addComponent(panelTolerance)
				.addGroup(gl_mainContent.createSequentialGroup()
						.addGap(10, 10, Short.MAX_VALUE)
						.addComponent(this.buttonReset)
						.addComponent(this.buttonApply)));
		
		gl_mainContent.setVerticalGroup(gl_mainContent.createSequentialGroup()
				.addComponent(panelVectorParam, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addComponent(panelTolerance, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addGap(10, 10, Short.MAX_VALUE)
				.addGroup(gl_mainContent.createParallelGroup(Alignment.CENTER)
						.addComponent(this.buttonReset)
						.addComponent(this.buttonApply)));
		// @formatter:on
	}

	private void initializeResources() {
		this.labelRecordCount.setText(DataViewProperties.getString("String_DatasetVectorPropertyControl_LabelRecordsetCount"));
		this.labelSpatialIndexType.setText(DataViewProperties.getString("String_DatasetVectorPropertyControl_LabelSpatialIndexType"));
		this.labelCharset.setText(DataViewProperties.getString("String_DatasetVectorPropertyControl_LabelCharset"));
		this.checkBoxIsReadOnly.setText(CoreProperties.getString(CoreProperties.ReadOnly));
		this.checkBoxIsFileCache.setText(DataViewProperties.getString("String_Property_IsFileCache"));
		this.buttonClearCache.setText(DataViewProperties.getString("String_DatasetVectorPropertyControl_CustomizeButtonClearCache"));
		this.labelNodeSnap.setText(DataViewProperties.getString("String_DatasetVectorPropertyControl_LabelToleranceFuzzy"));
		this.labelDangle.setText(DataViewProperties.getString("String_DatasetVectorPropertyControl_LabelToleranceDangle"));
		this.labelGrain.setText(DataViewProperties.getString("String_DatasetVectorPropertyControl_LabelToleranceGrain"));
		this.labelExtend.setText(DataViewProperties.getString("String_DatasetVectorPropertyControl_LabelToleranceExtend"));
		this.labelSmallPolygon.setText(DataViewProperties.getString("String_DatasetVectorPropertyControl_LabelToleranceSmallPolygon"));
		this.buttonDefaultTolerance.setText(CoreProperties.getString(CoreProperties.Default));
		this.buttonClearTolerance.setText(CoreProperties.getString(CoreProperties.Clear));
		this.buttonReset.setText(CommonProperties.getString(CommonProperties.Reset));
		this.buttonApply.setText(CommonProperties.getString(CommonProperties.Apply));
	}

	private void reset() {
		this.charset = this.datasetVector.getCharset();
		this.isReadOnly = this.datasetVector.isReadOnly();
		this.isFileCache = this.datasetVector.isFileCache();
		this.nodeSnap = this.datasetVector.getTolerance().getNodeSnap();
		this.grain = this.datasetVector.getTolerance().getGrain();
		this.smallPolygon = this.datasetVector.getTolerance().getSmallPolygon();
		this.dangle = this.datasetVector.getTolerance().getDangle();
		this.extend = this.datasetVector.getTolerance().getExtend();
	}

	private void fillComponents() {
		this.textFieldRecordCount.setText(Integer.toString(this.datasetVector.getRecordCount()));
		this.textFieldSpatialIndexType.setText(SpatialIndexTypeUtilties.toString(this.datasetVector.getSpatialIndexType()));
		this.comboBoxCharset.setSelectedItem(this.charset);
		this.checkBoxIsReadOnly.setSelected(this.isReadOnly);
		this.checkBoxIsFileCache.setSelected(this.isFileCache);
		this.textFieldNodeSnap.setValue(this.nodeSnap);
		this.textFieldGrain.setValue(this.grain);
		this.textFieldSmallPolygon.setValue(this.smallPolygon);
		this.textFieldDangle.setValue(this.dangle);
		this.textFieldExtend.setValue(this.extend);
	}

	private void registerEvents() {
		this.comboBoxCharset.addItemListener(this.itemListener);
		this.checkBoxIsReadOnly.addItemListener(this.itemListener);
		this.checkBoxIsFileCache.addItemListener(this.itemListener);
		this.textFieldNodeSnap.addPropertyChangeListener(ControlDefaultValues.PROPERTYNAME_VALUE, this.propertyChangeListener);
		this.textFieldGrain.addPropertyChangeListener(ControlDefaultValues.PROPERTYNAME_VALUE, this.propertyChangeListener);
		this.textFieldSmallPolygon.addPropertyChangeListener(ControlDefaultValues.PROPERTYNAME_VALUE, this.propertyChangeListener);
		this.textFieldDangle.addPropertyChangeListener(ControlDefaultValues.PROPERTYNAME_VALUE, this.propertyChangeListener);
		this.textFieldExtend.addPropertyChangeListener(ControlDefaultValues.PROPERTYNAME_VALUE, this.propertyChangeListener);
		this.buttonClearCache.addActionListener(this.actionListener);
		this.buttonDefaultTolerance.addActionListener(this.actionListener);
		this.buttonClearTolerance.addActionListener(this.actionListener);
		this.buttonReset.addActionListener(this.actionListener);
		this.buttonApply.addActionListener(this.actionListener);
	}

	private void unregisterEvents() {
		this.comboBoxCharset.removeItemListener(this.itemListener);
		this.checkBoxIsReadOnly.removeItemListener(this.itemListener);
		this.checkBoxIsFileCache.removeItemListener(this.itemListener);
		this.textFieldNodeSnap.removePropertyChangeListener(ControlDefaultValues.PROPERTYNAME_VALUE, this.propertyChangeListener);
		this.textFieldGrain.removePropertyChangeListener(ControlDefaultValues.PROPERTYNAME_VALUE, this.propertyChangeListener);
		this.textFieldSmallPolygon.removePropertyChangeListener(ControlDefaultValues.PROPERTYNAME_VALUE, this.propertyChangeListener);
		this.textFieldDangle.removePropertyChangeListener(ControlDefaultValues.PROPERTYNAME_VALUE, this.propertyChangeListener);
		this.textFieldExtend.removePropertyChangeListener(ControlDefaultValues.PROPERTYNAME_VALUE, this.propertyChangeListener);
		this.buttonClearCache.removeActionListener(this.actionListener);
		this.buttonDefaultTolerance.removeActionListener(this.actionListener);
		this.buttonClearTolerance.removeActionListener(this.actionListener);
		this.buttonReset.removeActionListener(this.actionListener);
		this.buttonApply.removeActionListener(this.actionListener);
	}

	private void setComponentsEnabled() {
		this.checkBoxIsFileCache.setEnabled(this.datasetVector.getDatasource().getEngineType() == EngineType.ORACLEPLUS
				&& this.datasetVector.getSpatialIndexType() == SpatialIndexType.TILE);
		this.buttonApply.setEnabled(verifyChange());
		this.buttonReset.setEnabled(verifyChange());
	}

	private boolean verifyChange() {
		return this.charset != this.datasetVector.getCharset() || this.isReadOnly != this.datasetVector.isReadOnly()
				|| this.isFileCache != this.datasetVector.isFileCache() || Double.compare(this.nodeSnap, this.datasetVector.getTolerance().getNodeSnap()) != 0
				|| Double.compare(this.grain, this.datasetVector.getTolerance().getGrain()) != 0
				|| Double.compare(this.smallPolygon, this.datasetVector.getTolerance().getSmallPolygon()) != 0
				|| Double.compare(this.dangle, this.datasetVector.getTolerance().getDangle()) != 0
				|| Double.compare(this.extend, this.datasetVector.getTolerance().getExtend()) != 0;
	}

	private void comboBoxCharsetSelectedChange(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.SELECTED) {
			this.charset = this.comboBoxCharset.getSelectedItem();
			setComponentsEnabled();
		}
	}

	private void checkBoxIsReadOnlyCheckedChange() {
		this.isReadOnly = this.checkBoxIsReadOnly.isSelected();
		setComponentsEnabled();
	}

	private void checkBoxIsFileCacheCheckedChange() {
		this.isFileCache = this.checkBoxIsFileCache.isSelected();
		setComponentsEnabled();
	}

	private void textFieldNodeSnapValueChange() {
		this.nodeSnap = (Double) this.textFieldNodeSnap.getValue();
		setComponentsEnabled();
	}

	private void textFieldGrainValueChange() {
		this.grain = (Double) this.textFieldGrain.getValue();
		setComponentsEnabled();
	}

	private void textFieldSmallPolygonValueChange() {
		this.smallPolygon = (Double) this.textFieldSmallPolygon.getValue();
		setComponentsEnabled();
	}

	private void textFieldDangleValueChange() {
		this.dangle = (Double) this.textFieldDangle.getValue();
		setComponentsEnabled();
	}

	private void textFieldExtendValueChange() {
		this.extend = (Double) this.textFieldExtend.getValue();
		setComponentsEnabled();
	}

	private void buttonClearCacheClicked() {
		// 暂未实现
	}

	private void buttonDefaultToleranceClicked() {
		Tolerance tolerance = new Tolerance(this.datasetVector.getTolerance());
		tolerance.setDefault();
		this.nodeSnap = tolerance.getNodeSnap();
		this.grain = tolerance.getGrain();
		this.smallPolygon = tolerance.getSmallPolygon();
		this.dangle = tolerance.getDangle();
		this.extend = tolerance.getExtend();
		setComponentsEnabled();
	}

	private void buttonClearToleranceClicked() {
		this.nodeSnap = 0.0;
		this.grain = 0.0;
		this.smallPolygon = 0.0;
		this.dangle = 0.0;
		this.extend = 0.0;
		setComponentsEnabled();
	}

	private void buttonResetClicked() {
		reset();
		fillComponents();
		setComponentsEnabled();
	}

	private void buttonApplyClicked() {
		if (this.datasetVector.getCharset() != this.charset) {
			this.datasetVector.setCharset(this.charset);
		}
		if (this.datasetVector.isReadOnly() != this.isReadOnly) {
			this.datasetVector.setReadOnly(this.isReadOnly);
		}
		if (this.datasetVector.isFileCache() != this.isFileCache) {
			this.datasetVector.setFileCache(this.isFileCache);
		}
		if (Double.compare(this.datasetVector.getTolerance().getNodeSnap(), this.nodeSnap) != 0) {
			this.datasetVector.getTolerance().setNodeSnap(this.nodeSnap);
		}
		if (Double.compare(this.datasetVector.getTolerance().getGrain(), this.grain) != 0) {
			this.datasetVector.getTolerance().setGrain(this.grain);
		}
		if (Double.compare(this.datasetVector.getTolerance().getSmallPolygon(), this.smallPolygon) != 0) {
			this.datasetVector.getTolerance().setSmallPolygon(this.smallPolygon);
		}
		if (Double.compare(this.datasetVector.getTolerance().getDangle(), this.dangle) != 0) {
			this.datasetVector.getTolerance().setDangle(this.dangle);
		}
		if (Double.compare(this.datasetVector.getTolerance().getExtend(), this.extend) != 0) {
			this.datasetVector.getTolerance().setExtend(this.extend);
		}
		setComponentsEnabled();
	}
}
