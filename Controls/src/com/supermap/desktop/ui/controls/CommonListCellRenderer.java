package com.supermap.desktop.ui.controls;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

public class CommonListCellRenderer implements ListCellRenderer<Object> {
	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		if (value instanceof JPanel) {
			if (isSelected) {// 设置选中时的背景色
				((JPanel) value).setBackground(Color.LIGHT_GRAY);
			} else {
				((JPanel) value).setBackground(Color.white);
			}
			return (JPanel) value;
		}
		return new JPanel();
	}

}