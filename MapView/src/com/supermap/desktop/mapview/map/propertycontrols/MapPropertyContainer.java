package com.supermap.desktop.mapview.map.propertycontrols;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.supermap.desktop.Application;
import com.supermap.desktop.FormMap;
import com.supermap.desktop.Interface.IFormMap;
import com.supermap.desktop.controls.ControlsProperties;
import com.supermap.desktop.event.ActiveFormChangedEvent;
import com.supermap.desktop.event.ActiveFormChangedListener;
import com.supermap.desktop.properties.CommonProperties;
import com.supermap.mapping.Map;

public class MapPropertyContainer extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JCheckBox checkBoxAutoApply;
	private JButton buttonApply;
	private JTabbedPane tabbledPane;
	private ArrayList<AbstractPropertyControl> propertyControls;

	/**
	 * Create the panel.
	 */
	public MapPropertyContainer() {
		initializeComponents();
		initializeResources();

		this.checkBoxAutoApply.setSelected(true);
		this.checkBoxAutoApply.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				checkBoxAutoApply();
			}
		});
		this.buttonApply.setEnabled(false);
		this.buttonApply.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonApplyClicked();
			}
		});
		Application.getActiveApplication().getMainFrame().getFormManager().addActiveFormChangedListener(new ActiveFormChangedListener() {

			@Override
			public void activeFormChanged(ActiveFormChangedEvent e) {
				if (e.getNewActiveForm() instanceof IFormMap) {
					setFormMap((IFormMap) e.getNewActiveForm());
				} else {
					setFormMap(null);
				}
			}
		});
	}

	public void setFormMap(IFormMap formMap) {
		if (propertyControls == null) {
			propertyControls = new ArrayList<AbstractPropertyControl>();
			propertyControls.add(new MapBasePropertyControl());
			propertyControls.add(new MapBoundsPropertyControl());
			propertyControls.add(new MapPrjCoordSysControl());
			for (AbstractPropertyControl abstractPropertyControl : propertyControls) {
				this.tabbledPane.addTab(abstractPropertyControl.getPropertyTitle(), abstractPropertyControl);
			}
		}

		if (formMap == null) {
			this.tabbledPane.setVisible(false);
		} else {
			this.tabbledPane.setVisible(true);
			for (AbstractPropertyControl abstractPropertyControl : propertyControls) {
				abstractPropertyControl.setMap(formMap.getMapControl().getMap());
			}
		}
		this.updateUI();
	}

	private void initializeComponents() {
		this.checkBoxAutoApply = new JCheckBox("AutoApply");
		this.buttonApply = new JButton("Apply");
		this.tabbledPane = new JTabbedPane();
		JPanel panelTabContainer = new JPanel();
		panelTabContainer.setLayout(new BorderLayout());
		panelTabContainer.add(this.tabbledPane, BorderLayout.CENTER);

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setAutoCreateContainerGaps(true);
		groupLayout.setAutoCreateGaps(true);
		this.setLayout(groupLayout);

		// @formatter:off
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(panelTabContainer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addGroup(groupLayout.createSequentialGroup()
						.addComponent(this.checkBoxAutoApply)
						.addGap(10, 20, Short.MAX_VALUE)
						.addComponent(this.buttonApply, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
		
		groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
				.addComponent(panelTabContainer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addGroup(groupLayout.createParallelGroup(Alignment.CENTER)
						.addComponent(this.checkBoxAutoApply)
						.addComponent(this.buttonApply)));
		// @formatter:on
	}

	private void initializeResources() {
		this.checkBoxAutoApply.setText(ControlsProperties.getString("String_AutoApply"));
		this.buttonApply.setText(CommonProperties.getString("String_Button_Apply"));
	}

	private void checkBoxAutoApply() {
		if (propertyControls != null && !propertyControls.isEmpty()) {
			for (AbstractPropertyControl abstractPropertyControl : propertyControls) {
				abstractPropertyControl.setAutoApply(this.checkBoxAutoApply.isSelected());
			}
			if (this.checkBoxAutoApply.isSelected()) {
				this.buttonApply.setEnabled(false);
			}
		} else {
			this.buttonApply.setEnabled(false);
		}
	}

	private void buttonApplyClicked() {
		if (propertyControls != null && !propertyControls.isEmpty()) {
			for (AbstractPropertyControl abstractPropertyControl : propertyControls) {
				abstractPropertyControl.apply();
			}
		}
		this.buttonApply.setEnabled(false);
	}
}
