package com.supermap.desktop.theme;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JFrame;

import com.supermap.desktop.Application;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.controls.DialogResult;
import com.supermap.desktop.ui.controls.DialogResultEvent;
import com.supermap.desktop.ui.controls.DialogResultListener;
import com.supermap.mapping.Layer;
import com.supermap.mapping.Theme;

/**
 * 专题图向导
 * 
 * @author WangDY
 * 
 */
class ThemeGuide extends JDialog {

	private transient Layer layer;
	private transient Theme theme;
	private transient ThemeMainPanel jPanelmain;
	private transient DialogResult dialogResult = DialogResult.CANCEL;
	private boolean modal = true;
	private transient Vector dialogResultListeners;
	private static CreateThemeContainer CONTAINER;

	private static void setContainer(CreateThemeContainer container) {
		CONTAINER = container;
	}

	/**
	 * 默认构造函数
	 */
	public ThemeGuide() {
		this(null, null);
	}

	/**
	 * 传入图层的构造函数
	 * 
	 * @param layer
	 *            需要制作专题图的图层
	 * 
	 */
	public ThemeGuide(Layer layer, CreateThemeContainer container) {
		super();
		this.layer = layer;
		CONTAINER = container;
	}

	/**
	 * 打开专题图向导对话框
	 * 
	 * @param Layer
	 *            需要制作专题图的图层
	 * 
	 * @return 返回用户制作的专题图，如果点击取消按钮，则返回null
	 */
	public static Theme openGuide(Layer layer) {
		Theme result = null;
		ThemeGuide guide = new ThemeGuide(layer, CONTAINER);
		DialogResult dialogResult = guide.showGuide();
		if (dialogResult == DialogResult.OK) {
			result = guide.getTheme();
		}
		return result;
	}

	/**
	 * 显示专题图向导
	 * 
	 * @param Layer
	 *            需要制作专题图的图层
	 * 
	 * @return 返回用户对当前专题图向导面板的操作，确定按钮表示OK，取消按钮表示CANCLE
	 */
	public DialogResult showGuide() {
		initialize();
		jPanelmain = new ThemeMainPanel(layer, this, CONTAINER);
		this.add(jPanelmain, BorderLayout.CENTER);
		this.setVisible(true);
		return dialogResult;
	}

	/**
	 * 初始化面板
	 */
	private void initialize() {

		super.setModal(modal);
		this.setSize(540, 435);
		this.setTitle(MapViewProperties.getString("String_ThemeGuide"));
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		try {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension frameSize = this.getSize();
			if (frameSize.height > screenSize.height) {
				frameSize.height = screenSize.height;
			}
			if (frameSize.width > screenSize.width) {
				frameSize.width = screenSize.width;
			}
			this.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 获取需要制作专题图的图层
	 * 
	 * @return 需要制作专题图的图层
	 */
	public Layer getLayer() {
		return layer;
	}

	/**
	 * 设置需要制作专题图的图层
	 * 
	 * @param layer
	 *            需要制作专题图的图层
	 */
	public void setLayer(Layer layer) {
		this.layer = layer;
	}

	/**
	 * 设置对话框是否为模态 Override
	 */
	@Override
	public void setModal(boolean modal) {
		this.modal = modal;
	}

	/**
	 * 添加释放方法，用来释放MapControl引起的未释放崩溃问题
	 */
	@Override
	public void dispose() {
		if (jPanelmain != null && jPanelmain.getBasicPanel() != null) {
			jPanelmain.getBasicPanel().disposeAll();
		}
	}

	/**
	 * 获取专题图对象
	 * 
	 * @return 专题图对象
	 */
	public Theme getTheme() {
		return theme;
	}

	/**
	 * 传回修改过的Theme
	 * 
	 * @param theme
	 */
	protected void setTheme(Theme theme) {
		this.theme = theme;
	}

	/**
	 * 传回向导面板最后点击的确定or取消
	 * 
	 * @param result
	 */
	protected void setResult(DialogResult result) {
		dialogResult = result;
		if (!dialogResult.equals(DialogResult.APPLY)) {
			this.setVisible(false);
		}
		fireDialogResult(new DialogResultEvent(this, dialogResult));
	}

	protected DialogResult getResult() {
		return dialogResult;
	}

	public synchronized void addDialogResultListener(DialogResultListener l) {
		if (dialogResultListeners == null) {
			dialogResultListeners = new Vector();
		}
		if (!dialogResultListeners.contains(l)) {
			dialogResultListeners.add(l);
		}
	}

	public synchronized void removeDialogResultListener(DialogResultListener l) {
		if (dialogResultListeners != null && dialogResultListeners.contains(l)) {
			dialogResultListeners.remove(l);
		}
	}

	protected void fireDialogResult(DialogResultEvent event) {
		if (dialogResultListeners != null) {
			Vector listeners = dialogResultListeners;
			int count = listeners.size();
			for (int i = 0; i < count; i++) {
				((DialogResultListener) listeners.elementAt(i)).dialogResultChanged(event);
			}
		}
	}
}
