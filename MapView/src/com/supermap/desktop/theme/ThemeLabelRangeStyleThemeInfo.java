package com.supermap.desktop.theme;

import java.util.ArrayList;

class ThemeLabelRangeStyleThemeInfo extends ThemeInfo {
	
	public ThemeLabelRangeStyleThemeInfo() {
		list = new ArrayList<Class<?>>();
		list.add(ThemeLabelRangeBasicPanel.class);
		list.add(ThemeLabelAttributePanel.class);
		list.add(ThemeLabelAdvancedPanel.class);
	}
}
