package com.supermap.desktop.utilties;

import com.supermap.desktop.Application;

public class StringUtilties {
	public static boolean isNullOrEmptyString(Object obj) {
		if (obj == null) {
			return true;
		}

		if (!(obj instanceof String)) {
			return true;
		}

		return ((String) obj).isEmpty();
	}

	public static boolean isNullOrEmpty(String str) {
		return (str == null || str.isEmpty());
	}

	public static boolean equals(String str1, String str2) {
		return equals(str1, str2, true);
	}

	public static boolean equals(String str1, String str2, boolean ignoreCase) {
		boolean result = false;

		try {
			// 同时为空，视为相同。null 和 空字符视为相同。
			if (isNullOrEmpty(str1) && isNullOrEmpty(str2)) {
				return true;
			}

			// 执行到这里时，表示不同时为空
			// 如果 str1 不为空，正常比较，得到结果
			// else str1 为空，那么 str2 一定不为空，result 保持 false 即可
			if (!isNullOrEmpty(str1)) {
				if (ignoreCase) {
					result = str1.equalsIgnoreCase(str2);
				} else {
					result = str1.equals(str2);
				}
			}
		} catch (Exception e) {
			Application.getActiveApplication().getOutput().output(e);
		}
		return result;
	}
	// /// <summary>
	// /// 取不超过指定字节数的字符串
	// /// </summary>
	// /// <param name="strSource">原字符串</param>
	// /// <param name="len">字节数</param>
	// public static String SubString(String strSource, int byteCount)
	// {
	// int intLength = 0;
	// char[] aryLenB = strSource.ToCharArray();
	// StringBuilder list = new StringBuilder();
	// foreach (char chrLenB in aryLenB)
	// {
	// if ((int)chrLenB > 255)
	// {
	// intLength += 2;
	// }
	// else
	// {
	// intLength++;
	// }
	//
	// if (intLength <= byteCount)
	// {
	// list.Append(chrLenB);
	// }
	// }
	// return list.ToString();
	// }
	//
	// /// <summary>
	// /// 为可输入控件提供过滤非法字符的功能，之后把代码换成正则表达式 [6/5/2012 wuxb]
	// /// </summary>
	// /// <param name="sourceText">源文本</param>
	// /// <param name="illegalChars">通用非法字符</param>
	// /// <param name="startIllegalChars">用以过滤字符串首的非法字符集</param>
	// /// <param name="endIllegalChars">用以过滤字符串末的非法字符集</param>
	// /// <returns></returns>
	// public static String FilterText(String sourceText, Char[]
	// illegalChars, Char[] startIllegalChars, Char[] endIllegalChars)
	// {
	// String text = sourceText;
	// try
	// {
	// if (illegalChars != null && illegalChars.Length > 0)
	// {
	// for (int i = 0; i < illegalChars.Length; i++)
	// {
	// int index = text.IndexOf(illegalChars.get(i));
	// if (index > -1)
	// {
	// text = text.Remove(index, 1);
	// }
	// }
	// }
	//
	// if (startIllegalChars != null && startIllegalChars.Length > 0)
	// {
	// for (int i = 0; i < startIllegalChars.Length; i++)
	// {
	// int index = text.IndexOf(startIllegalChars.get(i));
	// if (index == 0)
	// {
	// text = text.Remove(index, 1);
	// }
	// }
	// }
	//
	// if (endIllegalChars != null && endIllegalChars.Length > 0)
	// {
	// for (int i = 0; i < endIllegalChars.Length; i++)
	// {
	// int index = text.IndexOf(endIllegalChars.get(i));
	// if (index == text.Length - 1)
	// {
	// text = text.Remove(index, 1);
	// }
	// }
	// }
	// } catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// return text;
	// }
	//
	// /// <summary>
	// /// 字符串是否合法
	// /// </summary>
	// /// <param name="name">要判断的名字</param>
	// /// <param name="bAllowSpace">是否允许空格</param>
	// /// <returns></returns>
	// public static boolean IsLawName(String name, boolean bAllowSpace)
	// {
	// boolean isLaw = true;
	// try
	// {
	// List<Char> lawlessCode = new List<Char>();
	// lawlessCode.Add('/');
	// lawlessCode.Add('\\');
	// lawlessCode.Add('"');
	// lawlessCode.Add(':');
	// lawlessCode.Add('?');
	// lawlessCode.Add('*');
	// lawlessCode.Add('<');
	// lawlessCode.Add('>');
	// lawlessCode.Add('|');
	//
	// if (!bAllowSpace)
	// {
	// name = name.TrimStart();
	// }
	// isLaw = IsLawName(name, lawlessCode.ToArray());
	// } catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// return isLaw;
	// }
	//
	// /// <summary>
	// /// 字符串是否合法
	// /// </summary>
	// /// <param name="name">要判断的名字</param>
	// /// <param name="illegalChars">非法字符集合</param>
	// /// <returns></returns>
	// public static boolean IsLawName(String name, Char[] illegalChars)
	// {
	// boolean isLaw = true;
	// try
	// {
	// if (name.Length > 0)
	// {
	// for (int i = 0; i < illegalChars.Length; i++)
	// {
	// if (name.Contains(illegalChars.get(i).ToString()))
	// {
	// isLaw = false;
	// break;
	// }
	// }
	// }
	// else
	// {
	// isLaw = false;
	// }
	// } catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// return isLaw;
	// }
}
