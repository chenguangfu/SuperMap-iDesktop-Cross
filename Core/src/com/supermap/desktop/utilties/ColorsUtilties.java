package com.supermap.desktop.utilties;

import java.awt.Color;

import com.supermap.data.Colors;

public class ColorsUtilties {
	public static boolean isColorsEqual(Colors colors1, Colors colors2) {
		if (colors1 == null && colors2 != null) {
			return false;
		}

		if (colors1 != null && colors2 == null) {
			return false;
		}

		if (colors1.getCount() != colors2.getCount()) {
			return false;
		}

		for (int i = 0; i < colors1.getCount(); i++) {
			Color color1 = colors1.get(i);
			Color color2 = colors2.get(i);
			if (color1.getRGB() != color2.getRGB()) {
				return false;
			}
		}

		return true;
	}
}
