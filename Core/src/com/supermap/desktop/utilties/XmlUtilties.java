package com.supermap.desktop.utilties;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.supermap.desktop.Application;
import com.supermap.desktop._XMLTag;

public class XmlUtilties {
	public static Document getDocument(String filePath) {
		Document document = null;

		try {
			if (filePath != null && !filePath.isEmpty()) {
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = factory.newDocumentBuilder();
				File file = new File(filePath);
				document = documentBuilder.parse(file);
			}
		} catch (Exception e) {
			Application.getActiveApplication().getOutput().output(e);
		}
		return document;
	}

	public static Document getDocument(InputStream stream) {
		Document document = null;

		try {
			if (stream != null) {
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = factory.newDocumentBuilder();
				document = documentBuilder.parse(stream);
			}
		} catch (Exception e) {
			Application.getActiveApplication().getOutput().output(e);
		}
		return document;
	}

	/**
	 * 获取指定XML文档的根元素。
	 * 
	 * @param xmlFile
	 * @return
	 */
	public static Element getRootNode(String xmlFile) {
		Element rootElement = null;
		try {
			Document doc = getDocument(xmlFile);
			rootElement = doc.getDocumentElement();
			// 必须有我们的命名空间
			String xmlns = rootElement.getAttribute(_XMLTag.g_AttributionXMLNamespace);
			if (!xmlns.toLowerCase().equals(_XMLTag.g_ValueXMLNamespace)) {
				rootElement = null;
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
			rootElement = null;
		}
		return rootElement;
	}

	/**
	 * 将指定的Node写到指定的OutputStream流中。
	 * 
	 * @param os
	 *            将要写入的流。
	 * @param node
	 *            将要写入的节点。
	 * @param encoding
	 *            编码。
	 */
	public static void writeXml(OutputStream os, Node node, String encoding) throws TransformerException {
		TransformerFactory transFactory = TransformerFactory.newInstance();
		Transformer transformer = transFactory.newTransformer();
		transformer.setOutputProperty("indent", "yes");
		transformer.setOutputProperty(OutputKeys.ENCODING, encoding);

		DOMSource source = new DOMSource();
		source.setNode(node);
		StreamResult result = new StreamResult();
		result.setOutputStream(os);

		transformer.transform(source, result);
	}

	/**
	 * 将指定节点输出为字符串形式。
	 * 
	 * @param node
	 * @param encoding
	 * @return
	 * @throws TransformerException
	 * @throws UnsupportedEncodingException
	 */
	public static String nodeToString(Node node, String encoding) throws TransformerException, UnsupportedEncodingException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		writeXml(outputStream, node, encoding);
		return outputStream.toString(encoding);
	}

	/**
	 * 将指定的节点保存为 文件。
	 * 
	 * @param fileName
	 * @param node
	 * @param encoding
	 * @throws FileNotFoundException
	 * @throws TransformerException
	 */
	public static void saveXml(final String fileName, final Node node, String encoding) throws FileNotFoundException, TransformerException {
		writeXml(new FileOutputStream(fileName), node, encoding);
	}
}
