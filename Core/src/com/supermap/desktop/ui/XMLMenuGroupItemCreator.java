package com.supermap.desktop.ui;

import com.supermap.desktop.Interface.IDefaultValueCreator;
import com.supermap.desktop.Interface.IXMLCreator;
import com.supermap.desktop.enums.XMLCommandType;

public class XMLMenuGroupItemCreator implements IXMLCreator {

	private XMLMenuGroup parent = null;
	private IDefaultValueCreator defaultValueCreator = null;
	
	public XMLMenuGroupItemCreator(XMLMenuGroup parent) {
		this.parent = parent;
	}

	@Override
	public XMLCommandBase createElement(XMLCommandType commandType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IDefaultValueCreator getDefaultValueCreator() {
		// TODO Auto-generated method stub
		return this.defaultValueCreator; 
	}

	@Override
	public void setDefaultValueCreator(IDefaultValueCreator defaultValueCreator) {
		// TODO Auto-generated method stub
		this.defaultValueCreator = defaultValueCreator;
	}
//
//    public XMLCommandBase CreateElement(XMLCommandType commandType)
//    {
//        XMLCommand result = null;
//        try
//        {
//            PluginInfo pluginInfo = this.DefaultValueCreator.GetDefaultPluginInfo();
//            if (commandType == XMLCommandType.ContextMenuButton)
//            {
//                result = new XMLContextMenuButton(pluginInfo, this.parent);
//                (result as XMLContextMenuButton).Label = this.DefaultValueCreator.GetDefaultLabel("ContextMenuButton");
//                (result as XMLContextMenuButton).Index = this.DefaultValueCreator.GetDefaultIndex();
//            }
//            else if (commandType == XMLCommandType.ContextMenuButtonDropDown)
//            {
//                result = new XMLContextMenuButtonDropdown(pluginInfo, this.parent);
//                (result as XMLContextMenuButtonDropdown).Key = this.DefaultValueCreator.GetDefaultKey("ContextMenuButtonDropDown");
//                (result as XMLContextMenuButtonDropdown).Label = (result as XMLContextMenuButtonDropdown).Key;
//                (result as XMLContextMenuButtonDropdown).Index = this.DefaultValueCreator.GetDefaultIndex();
//            }
//        }
//        catch (System.Exception ex)
//        {
//            Application.ActiveApplication.Output.Output(ex.StackTrace, InfoType.Exception);
//        }
//
//        return result;
//    }
}
