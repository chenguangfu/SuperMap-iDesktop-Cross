package com.supermap.desktop.ui;

import org.w3c.dom.Element;

import com.supermap.desktop.Application;
import com.supermap.desktop.PluginInfo;
import com.supermap.desktop.enums.XMLCommandType;

public class XMLComboBox extends XMLCommand {

	private XMLCommand parent;
	public XMLComboBox(PluginInfo pluginInfo, XMLCommand parent) {
		super(pluginInfo);
		this.parent = parent;
		this.commandType = XMLCommandType.COMBOBOX;
	}

	@Override
	public boolean getIsContainer() {
		return false;
	}

	@Override
	public boolean initialize(Element xmlNodeCommand) {
		boolean result = false;
		try {
			result = super.initialize(xmlNodeCommand);
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return result;
	}

	@Override
	public XMLCommandBase clone(XMLCommandBase parent) {
		XMLComboBox result = null;
		try {
			result = (XMLComboBox) super.clone(parent);
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return result;
	}

	@Override
	public XMLCommandBase saveToPluginInfo(PluginInfo pluginInfo,
			XMLCommandBase parent) {
		XMLComboBox result = null;
		try {
			result = (XMLComboBox) super.saveToPluginInfo(pluginInfo, parent);
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return result;
	}

	// public override XmlElement ToXML(XmlDocument document)
	// {
	// XmlElement contextMenuButtonNode = null;
	// try
	// {
	// if (document != null)
	// {
	// contextMenuButtonNode = document.CreateElement(_XMLTag.g_ControlButton);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionIndex,
	// this.Index.ToString());
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionLabel,
	// this.Label);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionVisible,
	// this.Visible.ToString().ToLower());
	// Boolean isChecked = (this.CheckState ==
	// System.Windows.Forms.CheckState.Checked);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionCheckState,
	// isChecked.ToString().ToLower());
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionAssemblyName,
	// this.AssemblyFileName);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_OnAction,
	// this.CtrlActionClass);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionImage,
	// this.ImageFile);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionShortcutKey,
	// this.ShortCutKeys);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionScreenTip,
	// this.Tooltip);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionScreenTipImage,
	// this.TooltipImageFile);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionHelpURL,
	// this.HelpURL);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionCustomProperty,
	// this.CustomProperty);
	// SetAttributeCodeType(contextMenuButtonNode);
	// XmlCDataSection dynamicCodeNode = CreateDynamicCodeNode(document);
	// if (dynamicCodeNode != null)
	// {
	// contextMenuButtonNode.AppendChild(dynamicCodeNode);
	// }
	// }
	// }catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	//
	// return contextMenuButtonNode;
	// }
}
