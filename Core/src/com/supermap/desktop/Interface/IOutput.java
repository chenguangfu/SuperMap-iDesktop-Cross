package com.supermap.desktop.Interface;

import com.supermap.desktop.enums.InfoType;

public interface IOutput {

	/**
	* 获取指定索引的输出信息。
	*/
	String getLineText(int index);

	/**
	* 获取输出信息的条数。 
	*/
	int getLineCount();
	
	/**
	 * 是否可以复制输出信息
	 * @return 有选中内容返回true，否则返回false
	 */
	boolean canCopy();
	
	/**
	 * 复制选中的信息
	 */
	void copy();
	
	/**
	 * 是否可以清空输出窗口
	 * @return 有输出的内容返回true，否则返回false
	 */
	boolean canClear();
	
	/**
	 * 清空输出窗口
	 */
	void clear();
	
	/**
	* 获取或设置最大的输出信息条数。
	*/
	int getMaxLineCount();
	void setMaxLineCount(int maxCount);
	
	/**
	* 获取或设置最大的输出信息条数。
	*/
	Boolean getIsWordWrapped();
	void setIsWordWrapped(Boolean isWordWrapped);
	
	/**
	* 获取或设置是否在输出信息前添加信息输出的时间。
	*/
	Boolean getIsTimePrefixAdded();
	void setIsTimePrefixAdded(Boolean isTimePrefixAdded);
	
	/**
	* 获取或设置信息输出时间的格式。
	*/
	String getTimePrefixFormat();
	void setTimePrefixFormat(String timePrefixFormat);

	/**
	* 将指定的输出信息进行输出。 
	*/
	void output(String message);
	
	/**
	* 将指定的输出信息进行输出。 
	*/
	void output(Exception exception);
    
	/**
	* 将指定的输出信息进行输出。 
	*/
    void output(String message, InfoType type);
    
    /**
	* 清空输出信息。
	*/
    void clearOutput();        
   
//    /**
//	* 当进行输出信息的输出时将触发该事件。 
//	*/
//    event outputtingEventHandler Outputting;
}


///// <summary>
///// 表示 IOutput.Outputting 事件使用的委托类型。
///// </summary>
///// <param name="sender">事件源</param>
///// <param name="e">包含事件数据的 OutputEventArgs</param>
//public delegate void OutputtingEventHandler(object sender, OutputtingEventArgs e);
//
///// <summary>
///// 为 IOutput.Outputting 事件提供数据。
///// </summary>
//public class OutputtingEventArgs : CancelEventArgs
//{
//    /// <summary>
//    /// 构造一个新的 OutputEventArgs 对象。
//    /// </summary>
//    public OutputtingEventArgs()
//    {
//        this.message = String.Empty;
//    }
//
//    String this.message;
//    /// <summary>
//    /// 获取或设置当前输出信息的内容。
//    /// </summary>
//    public String Message
//    {
//        get
//        {
//            return this.message;
//        }
//        set
//        {
//            this.message = value;
//        }
//    }
//
//    InfoType this.type;
//    /// <summary>
//    /// 获取或设置当前输出信息的类型。
//    /// </summary>
//    public InfoType InfoType
//    {
//        get
//        {
//            return this.type;
//        }
//        set
//        {
//            this.type = value;
//        }
//    }
//}