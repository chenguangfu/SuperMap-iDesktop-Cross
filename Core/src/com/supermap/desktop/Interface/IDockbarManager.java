package com.supermap.desktop.Interface;

public interface IDockbarManager {

	/**
	 * 获取指定索引的浮动窗口。
	 */
	IDockbar get(int index);

	/**
	 * 获取指定索引的浮动窗口。
	 */
	IDockbar get(Class<?> controlClass);

	/**
	 * 获取应用程序内所包含的浮动窗口的总数。
	 */
	int getCount();

	/**
	 * 检查是否包含指定的浮动窗口。
	 * 
	 * @param dockBar
	 * @return
	 */
	boolean contains(IDockbar dockBar);
}
//
// /// <summary>
// /// 浮动窗口创建完成触发该事件。
// /// </summary>
// event EventHandler<DockBarEventArgs> DockBarCreatedEvent;
//
// /// <summary>
// /// 当浮动窗口关闭时将触发该事件。
// /// </summary>
// event EventHandler<CancelEventArgs> DockBarClosingEvent;
//
// /// <summary>
// /// 当浮动窗口关闭后将触发该事件。
// /// </summary>
// event EventHandler<EventArgs> DockBarClosedEvent;
//
// /// <summary>
// /// 当浮动窗口自动隐藏时将触发该事件。
// /// </summary>
// event EventHandler<CancelEventArgs> DockBarAutoHidingEvent;
//
// /// <summary>
// /// 当浮动窗口自动隐藏后将触发该事件。
// /// </summary>
// event EventHandler<EventArgs> DockBarAutoHidEvent;
// }
//
// public class DockBarEventArgs : EventArgs
// {
//
// IDockBar this.dockBar;
// internal DockBarEventArgs(IDockBar dockBar)
// {
// this.dockBar = dockBar;
// }
//
// public IDockBar DockBar
// {
// get
// {
// return this.dockBar;
// }
// }
// }
