package com.supermap.desktop.Interface;

import com.supermap.realspace.Layer3D;
import com.supermap.ui.SceneControl;

public interface IFormScene extends IForm {
	/**
	 * 获取场景窗口中的 SceneControl 控件。
	 * 
	 * @return
	 */
	SceneControl getSceneControl();

	/**
	 * 获取或设置场景窗口中所显示的三维图层的数组。
	 * 
	 * @return
	 */
	Layer3D[] getActiveLayer3Ds();

	void setActiveLayer3Ds(Layer3D[] activeLayer3Ds);

	// /// <summary>
	// /// 获取当前场景窗口中的对象绘制管理器。
	// /// </summary>
	// IGeometryDrawingManager GeometryDrawingManager
	// {
	// get;
	// }
	//
	// /// <summary>
	// /// ActiveLayer3Ds属性发生改变后将触发该事件。
	// /// </summary>
	// event EventHandler ActiveLayer3DsChanged;
	//
	// /// <summary>
	// /// ActiveLayer3Ds属性发生改变前将触发该事件。
	// /// </summary>
	// event ActiveLayer3DsChangingEventHandler ActiveLayer3DsChanging;
}
