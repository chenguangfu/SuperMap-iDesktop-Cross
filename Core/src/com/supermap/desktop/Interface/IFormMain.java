package com.supermap.desktop.Interface;

public interface IFormMain {

	/**
	 * Get the text
	 */
	String getText();

	/**
	 * Set the text
	 */
	void setText(String text);

	/**
	 * 获取子窗体管理器。
	 */
	IFormManager getFormManager();

	/**
	 * 获取窗体菜单管理器。
	 */
	IFrameMenuManager getFrameMenuManager();

	/**
	 * 获取右键菜单管理器。
	 */
	IContextMenuManager getContextMenuManager();

	/**
	 * 获取工具条管理器。
	 */
	IToolbarManager getToolbarManager();

	/**
	 * 获取浮动窗口管理器。
	 */
	IDockbarManager getDockbarManager();
	
	/**
	 * 获取状态栏管理器
	 * @return
	 */
	IStatusbarManager getStatusbarManager();

	/**
	 * Method doSomething documentation comment…
	 */
	void loadUI();
}
// /// <summary>
// /// 获取或设置窗口模式。
// /// </summary>
// MdiFormType MdiFormType
// {
// set;
// get;
// }
//
// /// <summary>
// /// 获取或设置是否显示主菜单
// /// </summary>
// Boolean IsFileTabVisible
// {
// set;
// get;
// }
//

//
// /// <summary>
// /// 获取子窗体状态栏管理器。
// /// </summary>
// IStatusBarManager StatusBarManager
// {
// get;
// }
//
// /// <summary>
// /// 获取最近打开文件列表管理器。
// /// </summary>
// IRecentFileManager RecentFileManager
// {
// get;
// }
//
// /// <summary>
// /// 获取 Ribbon 控件管理器。
// /// </summary>
// IRibbonManager RibbonManager
// {
// get;
// }
//
// /// <summary>
// /// 获取或设置Ribbon上StartMenu的显示模式。
// /// </summary>
// RibbonControlBoxMode RibbonControlBoxMode
// {
// get;
// set;
// }
//
// //Boolean TabbedMdi
// //{
// // get;
// // set;
// //}
//
// /// <summary>
// /// 获取窗口分组的管理器。
// /// </summary>
// IMdiTabGroupManager MdiTabGroupManager
// {
// get;
// }
//
// /// <summary>
// /// 获取或设置窗体的主题风格。
// /// </summary>
// ThemeStyle ThemeStyle
// {
// get;
// set;
// }
//
// /// <summary>
// /// 刷新主题风格
// /// </summary>
// /// <remarks>用户二次开发直接修改当前主题风格，而不是设置风格时使用.刷新过程中主题修改事件中的取消返回值无效</remarks>
// void RefreshThemeStyle();
//
// /// <summary>
// /// 加载主窗口界面元素后将触发该事件。
// /// </summary>
// event EventHandler Loaded;
//
// /// <summary>
// /// 加载主窗口界面元素时将触发该事件。
// /// </summary>
// event EventHandler Loading;
//
// /// <summary>
// /// 主题风格变化时将触发该事件。
// /// </summary>
// event ThemeStyleChangingEventHandler ThemeStyleChanging;
// /// <summary>
// /// 主题风格变化后将触发该事件。
// /// </summary>
// event ThemeStyleChangedEventHandler ThemeStyleChanged;