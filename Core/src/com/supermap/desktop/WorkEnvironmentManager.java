package com.supermap.desktop;

import java.io.File;
//import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
//import javax.xml.parsers.SAXParser;  
//import javax.xml.parsers.SAXParserFactory;  


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
//import org.xml.sax.SAXException;


import com.supermap.desktop.utilties.PathUtilties;
import com.supermap.desktop.utilties.XmlUtilties;

//import org.dom4j.DocumentException;  
//import org.dom4j.Element;  
//import org.dom4j.io.SAXReader;  
//import org.w3c.dom.Document;  
//import org.w3c.dom.Node;  
//import org.w3c.dom.NodeList;  
//import org.xml.sax.Attributes;  
//import org.xml.sax.InputSource;  
//import org.xml.sax.SAXException;  
//import org.xml.sax.helpers.DefaultHandler;  

public class WorkEnvironmentManager {

	private WorkEnvironmentCollection workEnvironments;
	private WorkEnvironment activeWorkEnvironment;
	private String workEnvrionmentRootPath;

	public WorkEnvironmentManager() {
		this.workEnvironments = new WorkEnvironmentCollection(this);
		this.workEnvrionmentRootPath = "";
		this.activeWorkEnvironment = null;

		initialize();
	}

	public Boolean initialize() {
		Boolean result = false;
		try {
			// 遍历所有工作环境
			String workEnvironmentPath = PathUtilties.getFullPathName(_XMLTag.g_FolderWorkEnvironment, true);
			File file = new File(workEnvironmentPath);
			File[] files = file.listFiles();
			for (int i = 0; i < files.length; i++) {
				List<String> plugins = new ArrayList<String>();

				if (files[i].isDirectory()) {
					String[] pathPrams = new String[] { workEnvironmentPath, files[i].getName() };
					workEnvironmentPath = PathUtilties.combinePath(pathPrams, true);

					File[] childFiles = files[i].listFiles();
					for (int j = 0; j < childFiles.length; j++) {
						// 读取配置文件字符串也用XML的接口去读，同时也要有我们的命名空间
						pathPrams = new String[] { workEnvironmentPath, childFiles[j].getName() };
						String configFile = PathUtilties.combinePath(pathPrams, false);
						Element pluginElement = XmlUtilties.getRootNode(configFile);
						if (pluginElement != null) {
							PluginInfo pluginInfo = new PluginInfo(pluginElement);
							if (pluginInfo.IsValid()) {
								// // 判断一下工作环境中是否含有必要的插件的配置信息
								if (pluginInfo.getBundleName().indexOf("SuperMap.Desktop.Frame") != -1
										|| pluginInfo.getBundleName().indexOf("SuperMap.Desktop.DataView") != -1) {
									plugins.add(pluginInfo.getBundleName());
								}
							}
						}
					}

					if (plugins.size() >= 2) {
						WorkEnvironment item = new WorkEnvironment(files[i].getName());
						this.workEnvironments.add(item);

						if (item.getName().toLowerCase().equals(_XMLTag.g_strWorkEnvironment.toLowerCase())) {
							this.activeWorkEnvironment = item;
						}
					}
				}
			}
		} catch (Exception ex) {

		}

		return result;
	}

	// public void SendReloadUIEvent()
	// {
	// if (ReloadUI != null)
	// {
	// ReloadUI(this, new EventArgs());
	// }
	// }

	public Boolean isWorkEnvironmentNameValid(String name) {
		Boolean valid = true;
		// List<Char> invalidPathChars = new List<char>();
		// invalidPathChars.AddRange(System.IO.Path.GetInvalidPathChars());
		// // 先判断名称是否非法
		// foreach (Char ch in name)
		// {
		// if (invalidPathChars.Contains(ch))
		// {
		// valid = false;
		// break;
		// }
		// }
		// // 在判断名称是否已经存在
		//
		// if (valid && Items.FindEnvironment(name) != null)
		// {
		// valid = false;
		// }

		return valid;
	}

	/**
	 * 获取或设置当前激活的工作环境的名称。
	 */
	public String getActiveWorkEnvironmentName() {
		String name = "";
		if (this.activeWorkEnvironment != null) {
			name = this.activeWorkEnvironment.getName();
		}
		return name;
	}

	public void setActiveWorkEnvironmentName(String name) {
		// WorkEnvironment newWorkEnvironment = this.workEnvironments.FindEnvironment(name);
		// this.ActiveWorkEnvironment = newWorkEnvironment;
	}

	/**
	 * 获取或设置当前激活的工作环境。
	 */
	public WorkEnvironment getActiveWorkEnvironment() {
		return this.activeWorkEnvironment;
	}

	public void setActiveWorkEnvironment(WorkEnvironment workEnvironment) {
		// 为了支持重新加载，不作相同工作环境的判断。这个判断可在外面调用前加上。
		Boolean excute = (workEnvironment != null);

		// if (excute)
		// {
		// if (ActiveWorkEnvironmentChanging != null)
		// {
		// ActiveWorkEnvironmentChangingEventArgs e = new ActiveWorkEnvironmentChangingEventArgs(value);
		// ActiveWorkEnvironmentChanging(this, e);
		// excute = !e.Cancel;
		// }
		// }
		//
		// if (excute)
		// {
		// WorkEnvironment oldWorkEnvironment = this.activeWorkEnvironment;
		// if (this.activeWorkEnvironment != null && this.activeWorkEnvironment.IsLoadded)
		// {
		// this.activeWorkEnvironment.UnLoad(this.activeWorkEnvironment.Name != value.Name);
		// }
		//
		// // 如果当前工作环境还没有被加载，就说明程序还在初始化过程中，只需要修改默认工作环境名字即可
		// if (this.activeWorkEnvironment != null && this.activeWorkEnvironment.IsLoadded)
		// {
		// this.activeWorkEnvironment = value;
		// }
		// else
		// {
		// this.activeWorkEnvironment = value;
		// this.activeWorkEnvironment.Load();
		// }
		//
		// if (ActiveWorkEnvironmentChanged != null)
		// {
		// ActiveWorkEnvironmentChangedEventArgs e = new ActiveWorkEnvironmentChangedEventArgs(oldWorkEnvironment, this.activeWorkEnvironment);
		// ActiveWorkEnvironmentChanged(this, e);
		// }
		// }
	}

	// public WorkEnvironment[] GetAllWorkEnvironments()
	// {
	// return this.workEnvironments.ToArray();
	// }
	//
	// /// <summary>
	// /// 获取工作环境集合对象
	// /// </summary>
	// public WorkEnvironmentCollection Items
	// {
	// get
	// {
	// return this.workEnvironments;
	// }
	// }
	//
	// #region Event
	// /// <summary>
	// /// 当前工作环境发生变化前将触发该事件。
	// /// </summary>
	// public event ActiveWorkEnvironmentChangingEventHandler ActiveWorkEnvironmentChanging;
	//
	// /// <summary>
	// /// 当前工作环境发生变化后将触发该事件。
	// /// </summary>
	// public event ActiveWorkEnvironmentChangedEventHandler ActiveWorkEnvironmentChanged;
	//
	// /// <summary>
	// /// 重新加载工作环境
	// /// </summary>
	// public event EventHandler ReloadUI;
	//
	// #endregion
	// }
	//
	// /// <summary>
	// /// 用于处理当前工作环境发生变化前的方法
	// /// </summary>
	// /// <param name="sender">WorkEnvironmentManager</param>
	// /// <param name="e">新的工作环境</param>
	// public delegate void ActiveWorkEnvironmentChangingEventHandler(object sender, ActiveWorkEnvironmentChangingEventArgs e);
	//
	// /// <summary>
	// /// 用于处理当前工作环境发生变化后的方法
	// /// </summary>
	// /// <param name="sender">WorkEnvironmentManager</param>
	// /// <param name="e">新的工作环境</param>
	// public delegate void ActiveWorkEnvironmentChangedEventHandler(object sender, ActiveWorkEnvironmentChangedEventArgs e);
	//
	// /// <summary>
	// /// 提供有关当前工作环境发生变化前的事件的数据
	// /// </summary>
	// public class ActiveWorkEnvironmentChangingEventArgs : CancelEventArgs
	// {
	// WorkEnvironment this.newWorkEnvironment;
	// /// <summary>
	// /// 初始化 SuperMap.Desktop.ActiveWorkEnvironmentChangingEventArgs 类的新实例。
	// /// </summary>
	// /// <param name="newActiveWorkEnvironment">变化后的工作环境对象实例</param>
	// public ActiveWorkEnvironmentChangingEventArgs(WorkEnvironment newActiveWorkEnvironment)
	// : base(false)
	// {
	// this.newWorkEnvironment = newActiveWorkEnvironment;
	// }
	//
	// /// <summary>
	// /// 变化前的工作环境对象实例
	// /// </summary>
	// public WorkEnvironment OldWorkEnvironment
	// {
	// get
	// {
	// return Application.ActiveApplication.WorkEnvironmentManager.ActiveWorkEnvironment;
	// }
	// }
	//
	// /// <summary>
	// /// 变化后的工作环境对象实例
	// /// </summary>
	// public WorkEnvironment NewWorkEnvironment
	// {
	// get
	// {
	// return this.newWorkEnvironment;
	// }
	// }
	// }
	//
	// /// <summary>
	// /// 提供有关当前工作环境发生变化后的事件的数据
	// /// </summary>
	// public class ActiveWorkEnvironmentChangedEventArgs : EventArgs
	// {
	// WorkEnvironment this.oldWorkEnvironment;
	// WorkEnvironment this.newWorkEnvironment;
	// /// <summary>
	// /// 初始化 SuperMap.Desktop.ActiveWorkEnvironmentChangedEventArgs 类的新实例。
	// /// </summary>
	// /// <param name="oldActiveWorkEnvironment">变化前的工作环境对象实例</param>
	// /// <param name="newActiveWorkEnvironment">变化后的工作环境对象实例</param>
	// public ActiveWorkEnvironmentChangedEventArgs(WorkEnvironment oldActiveWorkEnvironment, WorkEnvironment newActiveWorkEnvironment)
	// {
	// this.oldWorkEnvironment = oldActiveWorkEnvironment;
	// this.newWorkEnvironment = newActiveWorkEnvironment;
	// }
	//
	// /// <summary>
	// /// 变化前的工作环境对象实例
	// /// </summary>
	// public WorkEnvironment OldWorkEnvironment
	// {
	// get
	// {
	// return this.oldWorkEnvironment;
	// }
	// }
	//
	// /// <summary>
	// /// 变化后的工作环境对象实例
	// /// </summary>
	// public WorkEnvironment NewWorkEnvironment
	// {
	// get
	// {
	// return this.newWorkEnvironment;
	// }
	// }
}
