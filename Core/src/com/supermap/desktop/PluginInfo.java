package com.supermap.desktop;

import java.beans.XMLEncoder;
import java.io.Console;
import java.io.FileNotFoundException;

import javax.sql.rowset.spi.XmlWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.TransformerException;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.supermap.desktop.enums.XMLCommandType;
import com.supermap.desktop.ui.XMLMenus;
import com.supermap.desktop.ui.XMLDockbars;
import com.supermap.desktop.ui.XMLStatusbars;
import com.supermap.desktop.ui.XMLToolbar;
import com.supermap.desktop.ui.XMLToolbars;
import com.supermap.desktop.utilties.XmlUtilties;

public class PluginInfo {

	private Element frameMenusElement = null;
	private Element toolbarsElement = null;
	private Element statusbarsElement = null;
	private Element dockbarsElement = null;
	private Element contextMenusElement = null;

	public PluginInfo(Element element) {
		this.name = "";
		this.author = "";
		this.url = "";
		this.configLocation = "";
		this.bundleName = "";
		this.description = "";
		this.uiDefinition = "";
		this.hasRecentFile = false;
		this.namespaceURL = "";
		this.enable = true;
		this.FromConfig(element);
	}

	public PluginInfo(PluginInfo pluginInfo) {
		this.setName(pluginInfo.getName());
		// this.Author = pluginInfo.Author;
		// this.URL = pluginInfo.URL;
		// this.ConfigLocation = pluginInfo.ConfigLocation;
		// this.AssemblyName = pluginInfo.AssemblyName;
		// this.ClassName = pluginInfo.ClassName;
		// this.Description = pluginInfo.Description;
		// this.UIDefinition = pluginInfo.UIDefinition;
		// this.LoadOrder = pluginInfo.LoadOrder;
		// this.HasRecentFile = pluginInfo.HasRecentFile;
		// if (HasRecentFile)
		// {
		// this.RecentFile = pluginInfo.RecentFile;
		// }
		// this.namespaceURL = pluginInfo.this.namespaceURL;
		// this.enable = true;
		// this.xmlRibbon = pluginInfo.RibbonDefine.CopyTo(null) as XMLRibbon;
		// this.startMenu = pluginInfo.StartMenuDefines.CopyTo(null) as
		// XMLStartMenu;
		// this.xmlStatusBars = pluginInfo.StatusBarDefines.CopyTo(null) as
		// XMLStatusBars;
		// this.xmlContextMenus = pluginInfo.ContextMenuDefines.CopyTo(null) as
		// XMLContextMenus;
		// this.xmlDockbars = pluginInfo.DockbarDefines.CopyTo(null) as
		// XMLDockbars;
	}

	private static String NAMESPACEKEY = _XMLTag.g_AttributionXMLNamespace;
	String namespaceURL = "";

	public String getNamespaceURL() {
		return this.namespaceURL;
	}

	public void setNamespaceURL(String namespaceURL) {
		this.namespaceURL = namespaceURL;
	}

	private String name = "";

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private String author = "";

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	private String url = "";

	public String getURL() {
		return this.url;
	}

	public void setURL(String url) {
		this.url = url;
	}

	private String configLocation = "";

	public String getConfigLocation() {
		return this.configLocation;
	}

	public void setConfigLocation(String configLocation) {
		this.configLocation = configLocation;
	}

	private String bundleName = "";

	public String getBundleName() {
		return this.bundleName;
	}

	public void setBundleName(String bundleName) {
		this.bundleName = bundleName;
	}

	private String description = "";

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private String uiDefinition = "";

	public String getUIDefinition() {
		return this.uiDefinition;
	}

	public void setUIDefinition(String uiDefinition) {
		this.uiDefinition = uiDefinition;
	}

	private Boolean enable = false;

	public Boolean getEnable() {
		return this.enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	private Boolean hasRecentFile = false;

	public Boolean getHasRecentFile() {
		return this.hasRecentFile;
	}

	public void setHasRecentFile(Boolean hasRecentFile) {
		this.hasRecentFile = hasRecentFile;
	}

	private String recentFile = "";

	public String getRecentFile() {
		return this.recentFile;
	}

	public void setRecentFile(String recentFile) {
		this.recentFile = recentFile;
	}

	private Boolean isCurrent = false;

	public Boolean getIsCurrent() {
		return this.isCurrent;
	}

	public void setHasIsCurrent(Boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	private String helpLocalRoot = "";

	public String getHelpLocalRoot() {
		return this.helpLocalRoot;
	}

	public void setHelpLocalRoot(String helpLocalRoot) {
		this.helpLocalRoot = helpLocalRoot;
	}

	private String helpOnlineRoot = "";

	public String getHelpOnlineRoot() {
		return this.helpOnlineRoot;
	}

	public void setHelpOnlineRoot(String helpOnlineRoot) {
		this.helpOnlineRoot = helpOnlineRoot;
	}

	private XMLMenus xmlFrameMenus = null;

	public XMLMenus getFrameMenus() {
		return this.xmlFrameMenus;
	}

	// public void setFrameMenus(XMLFrameMenus xmlFrameMenus) {
	// this.xmlFrameMenus = xmlFrameMenus; }

	private XMLToolbars xmlToolbars = null;

	public XMLToolbars getToolbars() {
		return this.xmlToolbars;
	}

	// public void setXMLToolBars(XMLToolBars xmlToolBars) { this.xmlToolBars =
	// xmlToolBars; }

	public XMLStatusbars xmlStatusbars = null;

	public XMLStatusbars getStatusbars() {
		return this.xmlStatusbars;
	}

	public XMLDockbars xmlDockbars = null;

	public XMLDockbars getDockbars() {
		return this.xmlDockbars;
	}

	// public void setDockbars(XMLDockbars xmlDockbars) { this.xmlDockbars =
	// xmlDockbars; }

	private XMLMenus xmlContextMenus = null;

	public XMLMenus getContextMenus() {
		return this.xmlContextMenus;
	}

	// public void setXMLContextMenus(XMLContextMenus xmlContextMenus) {
	// this.xmlContextMenus = xmlContextMenus; }

	public Boolean IsValid() {
		Boolean valid = false;
		// valid = (this.assemblyName.Length > 0 && this.className.Length > 0);
		valid = true;
		return valid;
	}

	// int IComparable.CompareTo(Object pluginInfo)
	// {
	// int result = 0;
	// try
	// {
	// PluginInfo other = pluginInfo as PluginInfo;
	// result = this.LoadOrder - other.LoadOrder;
	// }
	// catch (System.Exception e)
	// {
	// Application.ActiveApplication.Output.Output(e.StackTrace,
	// InfoType.Exception);
	// }
	// return result;
	// }
	//
	public boolean FromConfig(Element element) {
		boolean result = false;

		try {
			try {
				this.setName(element.getAttribute(_XMLTag.g_AttributionName));
				this.setAuthor(element.getAttribute(_XMLTag.g_AttributionAuthor));
				this.setDescription(element.getAttribute(_XMLTag.g_AttributionDescription));
				this.setURL(element.getAttribute(_XMLTag.g_AttributionURL));
				this.setNamespaceURL(element.getNamespaceURI());
				this.setHelpLocalRoot(element.getAttribute(_XMLTag.g_AttributionHelpLocalRoot));
				this.setHelpOnlineRoot(element.getAttribute(_XMLTag.g_AttributionHelpOnlineRoot));
			} catch (Exception ex) {
				Application.getActiveApplication().getOutput().output(ex);
			}

			NodeList nodes = element.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {

				if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
					Element item = (Element) (nodes.item(i));

					if (item.getNodeName().equalsIgnoreCase(_XMLTag.g_Runtime)) {
						this.setBundleName(item.getAttribute(_XMLTag.g_AttributionBundleName));
						try {
							if (item.getAttribute(_XMLTag.g_AttributionEnabled).equalsIgnoreCase("false")) {
								this.setEnable(false);
							}
						} catch (Exception ex) {
							Application.getActiveApplication().getOutput().output(ex);
						}
					} else if (item.getNodeName().equalsIgnoreCase(_XMLTag.g_NodeFrameMenus)) {
						this.frameMenusElement = item;
					} else if (item.getNodeName().equalsIgnoreCase(_XMLTag.g_NodeToolbars)) {
						this.toolbarsElement = item;
					} else if (item.getNodeName().equalsIgnoreCase(_XMLTag.g_NodeStatusbars)) {
						this.statusbarsElement = item;
					} else if (item.getNodeName().equalsIgnoreCase(_XMLTag.g_NodeDockbars)) {
						this.dockbarsElement = item;
					} else if (item.getNodeName().equalsIgnoreCase(_XMLTag.g_NodeContextMenus)) {
						this.contextMenusElement = item;
					}
				}
			}

			if (this.IsValid()) {
				result = true;
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return result;
	}

	public Boolean parseUI() {

		this.xmlFrameMenus = new XMLMenus(this, XMLCommandType.FRAMEMENUS);
		this.xmlFrameMenus.load(this.frameMenusElement);

		this.xmlToolbars = new XMLToolbars(this);
		this.xmlToolbars.load(this.toolbarsElement);

		this.xmlContextMenus = new XMLMenus(this, XMLCommandType.CONTEXTMENUS);
		this.xmlContextMenus.load(this.contextMenusElement);

		this.xmlDockbars = new XMLDockbars(this);
		this.xmlDockbars.load(this.dockbarsElement);

		this.xmlStatusbars = new XMLStatusbars(this);
		this.xmlStatusbars.load(this.statusbarsElement);
		//
		// this.startMenu = new XMLStartMenu(this);
		// this.startMenu.Load(xmlDocument);

		return true;
	}

	public void toXML() {
		Element toolbarsElement = null;
		Document document = XmlUtilties.getDocument(this.configLocation);

		Element documentElement = document.getDocumentElement();
		NodeList nodeToolbars = documentElement.getElementsByTagName(_XMLTag.g_NodeToolbars);

		if (nodeToolbars != null && nodeToolbars.getLength() > 0) {
			toolbarsElement = (Element) nodeToolbars.item(0);
		}

		if (this.xmlToolbars != null && toolbarsElement != null) {
			NodeList childNodes = toolbarsElement.getChildNodes();

			for (int i = 0; i < childNodes.getLength(); i++) {
				Node childNode = childNodes.item(i);

				if (childNode != null && childNode.getNodeType() == Node.ELEMENT_NODE) {
					Element childElement = (Element) childNode;

					if (childElement.getNodeName().equalsIgnoreCase(_XMLTag.g_NodeToolbar)) {
						String id = childElement.getAttribute(_XMLTag.g_AttributionID);
						XMLToolbar xmlToolbar = this.xmlToolbars.getToolbar(id);

						if (xmlToolbar != null) {
							childElement.setAttribute(_XMLTag.g_AttributionIndex, Integer.toString(xmlToolbar.getIndex()));

							childElement.setAttribute(_XMLTag.g_AttributionVisible, Boolean.toString(xmlToolbar.getVisible()));
						}
					}
				}
			}

			try {
				XmlUtilties.saveXml(this.configLocation, document, document.getXmlEncoding());
			} catch (FileNotFoundException e) {
				Application.getActiveApplication().getOutput().output(e);
			} catch (TransformerException e) {
				Application.getActiveApplication().getOutput().output(e);
			}
		}
	}

	public String toString() {
		return this.getName();
	}
}
