package com.supermap.desktop;

import java.util.ArrayList;

import org.w3c.dom.Document;

public class ProcessPlugin {

	public static ArrayList<PluginInfo> processConfig()
    {
        ArrayList<PluginInfo> listInfos = new ArrayList<PluginInfo>();

//        try
//        {
//            // 查找workEnvironment节点，现在的配置文件是标准的xml文件，同时会加上我们的命名空间
//            String startupXml = CommonToolkit.PathWrap.GetFullPathName(_XMLTag.g_FileStartupXML);
//            XmlNode nodeWorkEnvironment = null;
//            XmlNode startupNode = CommonToolkit.XmlWrap.GetRootNode(startupXml);
//
//            foreach (XmlNode node in startupNode.ChildNodes)
//            {
//                if (node.Name.Equals(_XMLTag.g_NodeWorkEnvironment))
//                {
//                    nodeWorkEnvironment = node;
//                    break;
//                }
//            }
//
//            String defaultWorkEnvironment = nodeWorkEnvironment.Attributes[_XMLTag.g_AttributionDefault].Value;
//
//            String path = CommonToolkit.PathWrap.GetFullPathName(AppDomain.CurrentDomain.BaseDirectory + _XMLTag.g_FolderConfig + defaultWorkEnvironment);
//            String[] configFiles = Directory.GetFiles(path, _XMLTag.g_FileConfigExt, SearchOption.AllDirectories);
//            foreach (String configFile in configFiles)
//            {
//                try
//                {
//                    //读取配置文件字符串也用XML的接口去读，同时也要有我们的命名空间
//                    XmlNode pluginNode = CommonToolkit.XmlWrap.GetRootNode(configFile);
//                    if (pluginNode != null)
//                    {
//                        String content = pluginNode.OuterXml;
//                        PluginInfo pluginInfo = PluginInfo.FromConfig(content);
//                        if (pluginInfo != null)
//                        {
//                            pluginInfo.ConfigLocation = configFile;
//                            listInfos.Add(pluginInfo);
//                        }
//                    }
//                }
//                catch (Exception ex)
//                {
//                    Application.ActiveApplication.Output.Output(ex.StackTrace, InfoType.Exception);
//                }
//            }
//            listInfos.Sort();
//        }
//        catch (Exception ex)
//        {
//            Application.ActiveApplication.Output.Output(ex.StackTrace, InfoType.Exception);
//        }

        return listInfos;
    }

    /// <summary>
    /// 卸载当前工作环境中的插件
    /// </summary>
    /// <param name="isSaveConfig">是否保存工作环境的配置文件</param>
    /// <param name="reloadDockBar">是否重新加载浮动窗口</param>
    static public void unloadPlugins(Boolean isSaveConfig, Boolean reloadDockBar)
    {
//        //卸载的时候按照后加载先卸载的方式进行
//        Int32 index = -1;
//        if (isSaveConfig
//            && reloadDockBar)
//        {
//            for (Int32 i = 0; i < Application.ActiveApplication.WorkEnvironmentManager.ActiveWorkEnvironment.PluginInfos.Count; i++)
//            {
//                PluginInfo pluginInfo = Application.ActiveApplication.WorkEnvironmentManager.ActiveWorkEnvironment.PluginInfos[i];
//                pluginInfo.ReLoadDockBarInfo();
//            }
//            Application.ActiveApplication.WorkEnvironmentManager.ActiveWorkEnvironment.PluginInfos.ApplicationDockBars.Save();
//        }
//
//        while (Application.ActiveApplication.PluginManager.Count > 0)
//        {
//            index = Application.ActiveApplication.PluginManager.Count - 1;
//            Plugin plugin = Application.ActiveApplication.PluginManager[index];
//            Application.ActiveApplication.PluginManager.Unload(plugin, isSaveConfig, reloadDockBar);
//        }
    }

    public static Plugin createPluginFromConfigFile(String configFileName)
    {
        Plugin plugin = null;
//        try
//        {
//            XmlNode pluginNode = CommonToolkit.XmlWrap.GetRootNode(configFileName);
//            PluginInfo pluginInfo = PluginInfo.FromConfig(pluginNode.OuterXml);
//            plugin = Plugin.CreatePlugin(pluginInfo);
//        }
//        catch (System.Exception ex)
//        {
//            Application.ActiveApplication.Output.Output(ex.StackTrace, InfoType.Exception);
//            plugin = null;
//        }

        return plugin;
    }

    public static void saveToFile(PluginInfo pluginfo)
    {
//        try
//        {
//            String configFile = PluginInfo.ToConfig(pluginfo);
//            if (configFile != null && configFile != String.Empty)
//            {
//                String strText = SuperMap.Desktop.Properties.CoreResources.String_XMLVersion; // "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
//                configFile = strText + configFile;
//                XmlDocument doc = new XmlDocument();
//                doc.LoadXml(configFile);
//                SaveToFile(pluginfo.ConfigLocation, doc);
//            }
//        }
//        catch
//        {
//        }
    }

//    public static void saveToFile(String path, XmlDocument doc)
//    {
//        try
//        {
//            using (FileStream stream = File.Create(path))
//            {
//                XmlWriterSettings settings = new XmlWriterSettings();
//                settings.Indent = true;
//
//                XmlWriter writer = XmlWriter.Create(stream, settings);
//
//                doc.WriteTo(writer);
//                writer.Close();
//                stream.Close();
//                stream.Dispose();
//            }
//        }
//        catch (Exception ex)
//        {
//            Application.ActiveApplication.Output.Output(ex.StackTrace, InfoType.Exception);
//        }
//    }
}
