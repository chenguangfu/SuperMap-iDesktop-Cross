package com.supermap.desktop.CtrlAction.Dataset;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.supermap.data.Charset;
import com.supermap.data.Datasource;
import com.supermap.data.Datasources;
import com.supermap.data.EncodeType;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.dataeditor.DataEditorProperties;
import com.supermap.desktop.properties.CommonProperties;
import com.supermap.desktop.ui.controls.DataCell;
import com.supermap.desktop.ui.controls.DatasourceComboBox;
import com.supermap.desktop.ui.controls.mutiTable.component.MutiTable;
import com.supermap.desktop.ui.controls.mutiTable.component.MutiTableModel;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JComboBox;
import javax.swing.border.TitledBorder;

public class JDialogUnifiedSet extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JButton okButton = new JButton("OK");
	private JButton cancelButton = new JButton("Cancel");
	private final JPanel panel = new JPanel();
	private JPanel buttonPane;
	private JCheckBox checkboxTargetDatasource = new JCheckBox("targetDatasource");
	private JCheckBox checkboxEncodeType = new JCheckBox("encodeType");
	private JCheckBox checkboxCharset = new JCheckBox("charset");
	private transient DatasourceComboBox comboBoxTargetDatasource;
	private JComboBox<String> comboBoxEncodeType = new JComboBox<String>();
	private JComboBox<String> comboBoxCharset = new JComboBox<String>();
	private transient MutiTable table;

	public JDialogUnifiedSet(JDialogDatasetCopy owner, Boolean flag, MutiTable table) {
		super(owner, flag);
		this.table = table;
		setLocationRelativeTo(owner);
		initComponents();
		initResources();
	}

	public JDialogUnifiedSet() {
		initComponents();
		initResources();
	}

	private void initComponents() {

		initComboBox();
		setType(Type.UTILITY);
		setSize(372, 206);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButton_clicked();
			}
		});

		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButton_clcked();
			}
		});

		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
				.addComponent(buttonPane, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE));
		gl_contentPanel.setVerticalGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING).addGroup(
				gl_contentPanel.createSequentialGroup().addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(buttonPane, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)));
		contentPanel.setLayout(gl_contentPanel);

		comboBoxTargetDatasource.setEnabled(false);
		comboBoxEncodeType.setEnabled(false);
		comboBoxEncodeType.setEditable(true);
		comboBoxCharset.setEnabled(false);
		comboBoxCharset.setEditable(true);

		checkboxTargetDatasource.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				targetDatasource_changed();
			}
		});
		checkboxEncodeType.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				encodeType_changed();
			}
		});
		checkboxCharset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				charset_changed();
			}
		});
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setAutoCreateContainerGaps(true);
		gl_panel.setAutoCreateGaps(true);
		// checkboxTargetDatasource comboBoxTargetDatasource
		// checkboxEncodeType comboBoxEncodeType
		// checkboxCharset comboBoxCharset
		gl_panel.setAutoCreateContainerGaps(true);
		gl_panel.setAutoCreateGaps(true);
		gl_panel.setHorizontalGroup(gl_panel
				.createSequentialGroup()
				.addGroup(
						gl_panel.createParallelGroup(Alignment.LEADING).addComponent(checkboxTargetDatasource).addComponent(checkboxEncodeType)
								.addComponent(checkboxCharset))
				.addGap(36)
				.addGroup(
						gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(comboBoxTargetDatasource, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
								.addComponent(comboBoxEncodeType, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
								.addComponent(comboBoxCharset, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)));
		gl_panel.setVerticalGroup(gl_panel
				.createSequentialGroup()
				.addGroup(
						gl_panel.createParallelGroup().addComponent(checkboxTargetDatasource)
								.addComponent(comboBoxTargetDatasource, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(
						gl_panel.createParallelGroup().addComponent(checkboxEncodeType)
								.addComponent(comboBoxEncodeType, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(
						gl_panel.createParallelGroup().addComponent(checkboxCharset)
								.addComponent(comboBoxCharset, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)));
		panel.setLayout(gl_panel);
	}

	protected void cancelButton_clcked() {
		dispose();
	}

	protected void okButton_clicked() {
		int[] selectedRows = table.getSelectedRows();
		MutiTableModel tableModel = (MutiTableModel) table.getModel();
		List<Object> contents = tableModel.getContents();
		DataCell targetDatasource = (DataCell) comboBoxTargetDatasource.getSelectedItem();
		String encodeType = (String) comboBoxEncodeType.getSelectedItem();
		String charset = (String) comboBoxCharset.getSelectedItem();
		Object[][] datas = new Object[table.getRowCount()][6];
		for (int i = 0; i < table.getRowCount(); i++) {
			@SuppressWarnings("unchecked")
			Vector<Object> vector = (Vector<Object>) contents.get(i);
			datas[i][0] = vector.get(0);
			datas[i][1] = vector.get(1);
			datas[i][3] = vector.get(3);
			datas[i][2] = vector.get(2);
			datas[i][4] = vector.get(4);
			datas[i][5] = vector.get(5);
			// 用替换的方法实现统一设置
			for (int j = 0; j < selectedRows.length; j++) {
				int count = selectedRows[j];
				if (i == count) {
					Datasource tempDatasource = Application.getActiveApplication().getWorkspace().getDatasources().get(targetDatasource.toString());
					String tempDataset = tempDatasource.getDatasets().getAvailableDatasetName(vector.get(0).toString());
					datas[count][2] = targetDatasource;
					datas[count][3] = tempDataset;
					datas[count][4] = encodeType;
					datas[count][5] = charset;

				}
			}
		}
		try {
			tableModel.refreshContents(datas);
			table.updateUI();
			dispose();
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	protected void charset_changed() {
		boolean isSelected = checkboxCharset.isSelected();
		if (isSelected) {
			comboBoxCharset.setEnabled(true);
		} else {
			comboBoxCharset.setEnabled(false);
		}
	}

	protected void encodeType_changed() {
		boolean isSelected = checkboxEncodeType.isSelected();
		if (isSelected) {
			comboBoxEncodeType.setEnabled(true);
		} else {
			comboBoxEncodeType.setEnabled(false);
		}
	}

	protected void targetDatasource_changed() {
		boolean isSelected = checkboxTargetDatasource.isSelected();
		if (isSelected) {
			comboBoxTargetDatasource.setEnabled(true);
		} else {
			comboBoxTargetDatasource.setEnabled(false);
		}
	}

	/**
	 * 资源化
	 */
	private void initResources() {
		setTitle(DataEditorProperties.getString("String_GroupBox_ParameterSetting"));
		okButton.setText(CommonProperties.getString("String_Button_OK"));
		cancelButton.setText(CommonProperties.getString("String_Button_Cancel"));
		panel.setBorder(new TitledBorder(null, DataEditorProperties.getString("String_GroupBox_ParameterSetting"), TitledBorder.LEADING, TitledBorder.TOP,
				null, null));
		checkboxTargetDatasource.setText(CommonProperties.getString("String_ColumnHeader_TargetDatasource"));
		checkboxEncodeType.setText(CommonProperties.getString("String_ColumnHeader_EncodeType"));
		checkboxCharset.setText(DataEditorProperties.getString("String_Charset"));
	}

	/**
	 * 初始化panel中的JComboBox
	 */
	private void initComboBox() {
		// 目标数据源
		Datasources datasources = Application.getActiveApplication().getWorkspace().getDatasources();
		comboBoxTargetDatasource = new DatasourceComboBox(datasources);
		// 编码类型
		ArrayList<String> encodeType = new ArrayList<String>();
		encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.NONE));
		encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.BYTE));
		encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.INT16));
		encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.INT24));
		encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.INT32));
		encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.DCT));
		encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.SGL));
		encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.LZW));
		encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.PNG));
		encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.COMPOUND));
		comboBoxEncodeType.setModel(new DefaultComboBoxModel<String>(encodeType.toArray(new String[encodeType.size()])));
		// 字符集
		ArrayList<String> charsetes = new ArrayList<String>();
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.OEM));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.EASTEUROPE));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.THAI));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.RUSSIAN));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.BALTIC));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.ARABIC));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.HEBREW));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.VIETNAMESE));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.TURKISH));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.GREEK));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.CHINESEBIG5));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.JOHAB));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.HANGEUL));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.SHIFTJIS));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.MAC));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.SYMBOL));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.DEFAULT));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.ANSI));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.UTF8));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.UTF7));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.WINDOWS1252));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.KOREAN));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.UNICODE));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.CYRILLIC));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5GERMAN));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5SWEDISH));
		charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5NORWEGIAN));
		comboBoxCharset.setModel(new DefaultComboBoxModel<String>(charsetes.toArray(new String[charsetes.size()])));
		comboBoxCharset.setSelectedIndex(18);
	}
}
